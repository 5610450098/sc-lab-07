import java.util.ArrayList;


public class TheaterManagement {
	private double ticketPrice[][];
	
	
	public TheaterManagement(double[][] ticketPrices){
		this.ticketPrice = ticketPrices.clone();
	}
	
	
	public double chooseSeat(String rollStr, int seat){
		int roll = (int) rollStr.charAt(0)-65;
		seat = seat-1;
		double prices = this.ticketPrice[roll][seat];
		//ticketPrices[roll][seat] = 0;
		return prices;
	}
	public double buyTicket(String rollStr, int seat){
		double prices = chooseSeat(rollStr,seat);
		this.ticketPrice[(int)rollStr.charAt(0)-65][seat-1] = 0;
		return prices;
	}
	public String choosePrice(int price){
		double prices = (double) price;
		String ticket = "empty seat "+price+" $\n";
		int k = 0;
		for(int i=0;i<this.ticketPrice.length;i++){
			for(int j=0;j<this.ticketPrice[i].length;j++){
				if(this.ticketPrice[i][j]==prices){
					String s = "";
					char roll = (char)(i+65);
					int seat = j+1;
					s = s+roll+seat;
					ticket = ticket + s + " ";
					//this.ticketPrice[i][j] = 0;
					k++;
				}
			}
			if(k!=0){
				ticket = ticket+"\n";
				k=0;
			}
			
		}
		return ticket;
	}
	public double[][] setTicket(){
		return this.ticketPrice;
	}
}
