import java.net.SecureCacheResponse;
import java.util.ArrayList;
import java.util.Scanner;


public class ControlTheater {
	private ArrayList<TheaterManagement> dayRound = new ArrayList<TheaterManagement>();
	
	public ControlTheater(){
		for(int i=0;i<7;i++){
			for(int j=0;j<3;j++){
				if(j==2){
					if(i==0 || i==6){
//						TheaterManagement theaterData = new TheaterManagement(DataSeatPrice.ticketPrices);
						dayRound.add(new TheaterManagement(DataSeatPrice.ticketPrices));
					}
				}
				else{
//					TheaterManagement theaterData = new TheaterManagement(DataSeatPrice.ticketPrices);
					dayRound.add(new TheaterManagement(DataSeatPrice.ticketPrices));
				}
			}
		}
		TestCase();
		run();
	}
	
	public static void main(String[] args){
		new ControlTheater();
	}
	
	public int setDayround(String s){
		int day = 0;
		String[] Dayround = new String[] {"Sunday_Round1","Sunday_Round2","Sunday_Round3",
				 "Monday_Round1","Monday_Round2","Tuesday_Round1","Tuesday_Round2",
				"Wednesday_Round1","Wednesday_Round2","Thursday_Round1","Thursday_Round2",
				 "Friday_Round1","Friday_Round2","Saturday_Round1","Saturday_Round2","Saturday_Round3"};
		for(int i=0;i<Dayround.length;i++){
			if(Dayround[i]==s){
				day = i;
			}
		}
		return day;
	}
	public void run(){
		Scanner sc = new Scanner(System.in);
//		System.out.println("Theater\n1.Reserve Ticket\n2. Buy Ticket\n3.Exit");
		boolean status = true;
		while(status){
			System.out.println("\nTheater\n1.Reserve Ticket\n2.Buy Seat Ticket\n3.Buy Price Ticket\n4.Exit");	
			String choice = sc.next();
			if(choice.equals("4")){
				status = false;
			}
			else{
				boolean statusDate = true;
				while(statusDate){
					System.out.println("Choose Date and Round ?\tex.Sunday_Round1 Monday_Round2 ...Mon-Fri have2round Sat-Sun have3round");
					String date = sc.next();
					if(choice.equals("3")){
						System.out.println("Choose Price of seat?");
						int price = sc.nextInt();
						System.out.println(dayRound.get(setDayround(date)).choosePrice(price));
					}
					else{
						System.out.println("How many Reserve/Buy Ticket ?");
						int nTicket = sc.nextInt();
						for(int j=0;j<nTicket;j++){
						System.out.println("Choose Seat");
						String seat = sc.next();
							if(choice.equals("2")){
								if(dayRound.get(setDayround(date)).chooseSeat(seat.substring(0, 1),Integer.parseInt(seat.substring(1, seat.length()))) != 0){
									System.out.println(dayRound.get(setDayround(date)).buyTicket(seat.substring(0, 1),Integer.parseInt(seat.substring(1, seat.length()))));
								}
								else{
									System.out.println("Please choose ticket again !!!");
									j--;
								}								}
							else {
								System.out.println(dayRound.get(setDayround(date)).chooseSeat(seat.substring(0, 1),Integer.parseInt(seat.substring(1, seat.length()))));
							}
						}
					}
					System.out.println("Do you want back to menu? Please pull (r)");
					String backMenu = sc.next();
					if (backMenu.equals("r") || backMenu.equals("R")){
						statusDate = false;
					}
					
				}
				
			}
		}
		
	}
	
	public void TestCase(){
		System.out.println("-------Test A------");
		 System.out.println("Buy ticket(s) First Round of Sunday :  O11 , O12");
		 double a = dayRound.get(0).buyTicket("O", 11);
		 double b = dayRound.get(0).buyTicket("O", 12);
		 System.out.println("O11 :"+a);
		 System.out.println("O12 :"+b);
		 System.out.println("total :"+(a+b));
		 
		 System.out.println("\n"+"-------Test B------");
		 System.out.println("Buy ticket(s) Prices 20$ and 50$");
		 System.out.println(dayRound.get(0).choosePrice(20));
		 System.out.println("choose D7 : "+dayRound.get(0).buyTicket("D", 7)+"\n");
		 System.out.println(dayRound.get(0).choosePrice(50));
		 System.out.println("choose M5 : "+dayRound.get(0).buyTicket("M", 5)+"\n");
		 
		 
		 System.out.println("\n"+"-------Test C------");
		 System.out.println("Buy ticket(s) First Round of Sunday :  O12 , O13");
		 System.out.println(dayRound.get(0).buyTicket("O", 12));
		 System.out.println(dayRound.get(0).buyTicket("O", 13));
		 
	}
	
	
	
	
	
	
}
